;;; oshi_at-uploader.el -*- lexical-binding: t -*-
;;; --- simple function for uploading the file on "oshi.at"

;; Author: Richard F. <fela@airmail.cc>
;; Keywords: oshi.at, curl, uploader

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This function is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.



;;; Code:

(defun upload-file-oshi-at (file-path)
  "Uploads a file to oshi.at using async-shell-command; asking for filename and expiration time (in days)"
  (interactive "FEnter file (max. ~5 GB): ")  
  (let* ((expire-time (read-number "Enter expiration time (days): "))
	 (exp-time-days (* 1440 expire-time))
	 (upload-command (format "curl https://oshi.at -F f=@%s -F expire=%s " file-path exp-time-days))
         (output-buffer-name "*Oshi.at Upload Result*"))
    (generate-new-buffer output-buffer-name)
    (switch-to-buffer output-buffer-name)
    (async-shell-command upload-command (current-buffer))
    (insert "\nUploading, please wait…")
    (insert "\ncURL command: " upload-command)
    (read-only-mode)
    ))
